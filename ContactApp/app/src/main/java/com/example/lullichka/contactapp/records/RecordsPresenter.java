package com.example.lullichka.contactapp.records;

import android.util.Log;

import com.example.lullichka.contactapp.data.RecordRepoInterface;
import com.example.lullichka.contactapp.data.RecordRepository;
import com.example.lullichka.contactapp.model.Record;

import java.util.List;

/**
 * Created by lullichka on 13.10.16.
 */
public class RecordsPresenter implements RecordsContract.UserActionsListener {

    private static final String LOG_TAG = RecordsPresenter.class.getSimpleName();
    private final RecordRepository mRecordRepository;
    private final RecordsContract.View mRecordsView;


    public RecordsPresenter(RecordRepository recordRepository, RecordsContract.View recordsView){
        mRecordRepository = recordRepository;
        mRecordsView = recordsView;
    }
    @Override
    public void loadRecords() {
        mRecordRepository.getRecords( new RecordRepoInterface.LoadRecordsCallback() {
            @Override
            public void onRecordsLoaded(List<Record> records) {
                mRecordsView.showRecords(records);
            }
        });
    }

    @Override
    public void deleteRecord( Record record) {
        mRecordRepository.deleteRecord(record);
        Log.v(LOG_TAG, "delete "+record.getId());
    }

    @Override
    public void openRecordDetails(Record record) {
        mRecordsView.showRecordDetail(record);
    }
}