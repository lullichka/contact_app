package com.example.lullichka.contactapp.addrecord;

import com.example.lullichka.contactapp.data.RecordRepository;
import com.example.lullichka.contactapp.model.Record;

/**
 * Created by lullichka on 04.10.16.
 */
public class AddRecordsPresenter implements AddRecordContract.UserActionsListener {

    private final RecordRepository mRecordRepository;
    private final AddRecordContract.View mAddRecordView;

    public AddRecordsPresenter (RecordRepository recordRepository, AddRecordContract.View addRecordsView){
        mRecordRepository = recordRepository;
        mAddRecordView = addRecordsView;
    }

    @Override
    public void addRecord(String id, String name, String email, String phone) {
        Record newRecord = new Record(id, name, email, phone);
        if (newRecord.isEmpty()){
            mAddRecordView.showEmptyRecordError();
        } else {
            mRecordRepository.addRecord(newRecord);
            mAddRecordView.showRecordsList();
        }
    }

    @Override
    public void updateRecord( String id, String name, String email, String phone) {
        Record updatedRecord = new Record(id, name, email, phone);
        if (updatedRecord.isEmpty()){
            mAddRecordView.showEmptyRecordError();
        } else {
            mRecordRepository.updateRecord( updatedRecord);
            mAddRecordView.showRecordsList();
        }
    }

}
