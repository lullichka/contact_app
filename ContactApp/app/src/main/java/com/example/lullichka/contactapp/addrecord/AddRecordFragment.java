package com.example.lullichka.contactapp.addrecord;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lullichka.contactapp.R;
import com.example.lullichka.contactapp.data.RecordRepository;

/**
 * Created by lullichka on 16.09.16.
 */
public class AddRecordFragment extends Fragment implements AddRecordContract.View {
    private static final String LOG_TAG = AddRecordFragment.class.getSimpleName();
    private static final String ARGUMENT_NAME = "name";
    private static final String ARGUMENT_PHONE = "phone";
    private static final String ARGUMENT_EMAIL = "email";
    private static final String ARGUMENT_ID = "id";
    private EditText etName;
    private EditText etEmail;
    private EditText etPhone;
    private AddRecordContract.UserActionsListener mActionsListener;
    private String mId;

    public AddRecordFragment() {
        // Required empty public constructor
    }

    public static AddRecordFragment newInstance(String id, String name, String email, String phone) {
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_ID, id);
        arguments.putString(ARGUMENT_NAME, name);
        arguments.putString(ARGUMENT_EMAIL, email);
        arguments.putString(ARGUMENT_PHONE, phone);
        AddRecordFragment f = new AddRecordFragment();
        f.setArguments(arguments);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            String name = args.getString(ARGUMENT_NAME);
            String email = args.getString(ARGUMENT_EMAIL);
            String phone = args.getString(ARGUMENT_PHONE);
            mId = args.getString(ARGUMENT_ID);
            etName.setText(name);
            etEmail.setText(email);
            etPhone.setText(phone);
            Log.v(LOG_TAG, "id is "+mId);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_add, container, false);
        mActionsListener = new AddRecordsPresenter(new RecordRepository(), this);
        etName = (EditText) rootView.findViewById(R.id.editTextDate);
        etEmail = (EditText) rootView.findViewById(R.id.editTextWeight);
        etPhone = (EditText) rootView.findViewById(R.id.editTextDesc);
        FloatingActionButton floatingActionButton = (FloatingActionButton) getActivity().findViewById(R.id.fab);
        if (floatingActionButton != null) {
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mId==null) {
                        mActionsListener.addRecord(
                                mId,
                                etName.getText().toString(),
                                etEmail.getText().toString(),
                                etPhone.getText().toString());
                    } else {
                        mActionsListener.updateRecord(
                                mId,
                                etName.getText().toString(),
                                etEmail.getText().toString(),
                                etPhone.getText().toString());
                    }
                }
            });
        }
        return rootView;
    }

    @Override
    public void showRecordsList() {
        getActivity().finish();
    }

    @Override
    public void showEmptyRecordError() {
        Toast.makeText(getActivity(), "Contact cannot be empty!", Toast.LENGTH_LONG).show();
    }
//TODO make onchangeOrientation
}
