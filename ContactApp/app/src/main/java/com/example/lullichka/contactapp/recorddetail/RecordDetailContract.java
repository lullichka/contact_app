package com.example.lullichka.contactapp.recorddetail;

import com.example.lullichka.contactapp.model.Record;

/**
 * Created by lullichka on 03.10.16.
 */
public interface RecordDetailContract {
    interface View {
        void showName(String name);

        void showEmail(String email);

        void showPhone(String phone);
    }

    interface UserActionsListener {

        void openRecord(Record record);

        void updateRecord(Record record);
    }
}
