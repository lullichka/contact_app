package com.example.lullichka.contactapp.data;

import android.util.Log;

import com.example.lullichka.contactapp.model.Record;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lullichka on 14.10.16.
 */
public class RecordRepository implements RecordRepoInterface {
    private static final String TAG = RecordRepository.class.getSimpleName();
    private static final String RECORD_CHILD = "record";
    private DatabaseReference mDatabaseReference;

    @Override
    public void getRecords(final LoadRecordsCallback callback) {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(RECORD_CHILD);
        mDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Record> records = new ArrayList<>();
                for (DataSnapshot recordshapshot : dataSnapshot.getChildren()) {
                    Record record = recordshapshot.getValue(Record.class);
                    records.add(record);
                    callback.onRecordsLoaded(records);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }


    @Override
    public void addRecord(Record record) {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(RECORD_CHILD);
        DatabaseReference reference = mDatabaseReference.push();
        record.setId(reference.getKey());
        reference.setValue(record);
        Log.d(TAG, "Record " + record.getId() + " pushed");
    }

    @Override
    public void deleteRecord(final Record record) {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(RECORD_CHILD);
        Log.d(TAG, mDatabaseReference.toString());
        mDatabaseReference.child(record.getId()).removeValue();
        Log.d(TAG, "Record " + record.getId() + " removed");
    }

    @Override
    public void updateRecord(Record record) {
        mDatabaseReference = FirebaseDatabase.getInstance().getReference(RECORD_CHILD);
        DatabaseReference reference = mDatabaseReference.child(record.getId());
        reference.setValue(record);
        Log.d(TAG, "Record " + record.getId() + " updated");
    }
}
