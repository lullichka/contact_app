package com.example.lullichka.contactapp.records;

import com.example.lullichka.contactapp.model.Record;

import java.util.List;

/**
 * Created by lullichka on 13.10.16.
 */
public interface RecordsContract {
    interface UserActionsListener {
        void loadRecords();

        void deleteRecord(Record record);

        void openRecordDetails(Record record);

    }

    interface View {
        void showRecords(List<Record> records);

        void showRecordDetail(Record record);
    }
}