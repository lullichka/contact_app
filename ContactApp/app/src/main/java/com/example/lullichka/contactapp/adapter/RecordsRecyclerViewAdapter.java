package com.example.lullichka.contactapp.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lullichka.contactapp.R;
import com.example.lullichka.contactapp.model.Record;
import com.example.lullichka.contactapp.records.MainFragment;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by lullichka on 13.10.16.
 */

    public class RecordsRecyclerViewAdapter extends RecyclerView.Adapter<RecordsRecyclerViewAdapter.RecordViewHolder> {

        private static final String LOG_TAG = RecordsRecyclerViewAdapter.class.getSimpleName();
        private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec
        private MainFragment.RecordItemListener mRecordItemListener;

        private Context mContext;
        private List<Record> mRecordsList;
        private List<Record> mPendingRemovalList;

        private Handler handler = new Handler(); // hanlder for running delayed runnables
        HashMap<Record, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be


        public RecordsRecyclerViewAdapter(Context context, List<Record> recordList,
                                          MainFragment.RecordItemListener recordItemListener) {
            setList(recordList);
            mRecordItemListener = recordItemListener;
            mContext = context;
            mRecordsList = recordList;
            mPendingRemovalList = new ArrayList<>();
        }

        public void setList(List<Record> recordList) {
            mRecordsList = recordList;
        }

        public void replaceData(List<Record> recordList) {
            setList(recordList);
            notifyDataSetChanged();
        }

        @Override
        public RecordViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            Context context = parent.getContext();
            LayoutInflater inflater = LayoutInflater.from(context);
            View recordView = inflater.inflate(R.layout.record_recyclerview_item, parent, false);
            return new RecordViewHolder(recordView, mRecordItemListener);
        }

        @Override
        public void onBindViewHolder(RecordViewHolder holder, int position) {
            RecordViewHolder viewHolder =  holder;
            final Record item = mRecordsList.get(position);
            if (mPendingRemovalList.contains(item)) {
                // we need to show the "undo" state of the row
                holder.cardView.setBackgroundColor(Color.argb(255, 	63, 81, 181));
                holder.textViewName.setVisibility(View.GONE);
                holder.imageView.setVisibility(View.GONE);
                holder.undoButton.setVisibility(View.VISIBLE);
                holder.undoButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // user wants to undo the removal, let's cancel the pending task
                        Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                        pendingRunnables.remove(item);
                        if (pendingRemovalRunnable != null)
                            handler.removeCallbacks(pendingRemovalRunnable);
                        mPendingRemovalList.remove(item);
                        // this will rebind the row in "normal" state
                        notifyItemChanged(mRecordsList.indexOf(item));
                    }
                });
            } else {
                Record record = mRecordsList.get(position);
                holder.textViewName.setText(record.getName());
                holder.undoButton.setVisibility(View.GONE);
                holder.undoButton.setOnClickListener(null);
                String http = "http:";
                if (record.getPhoto() != null) {
                    String photoAddress = http.concat(record.getPhoto());
                    Picasso.with(mContext)
                            .load(photoAddress)
                            .centerCrop()
                            .fit()
                            .placeholder(R.drawable.placeholder)
                            .into(holder.imageView);
                }
            }
        }

        @Override
        public int getItemCount() {
            return mRecordsList == null ? 0 : mRecordsList.size();
        }

        class RecordViewHolder extends RecyclerView.ViewHolder {
            ImageView imageView = (ImageView) itemView.findViewById(R.id.image);
            TextView textViewName = (TextView) itemView.findViewById(R.id.tv_name);
            Button undoButton = (Button) itemView.findViewById(R.id.undo_button);
            CardView cardView = (CardView) itemView.findViewById(R.id.card_view);

            public RecordViewHolder(final View itemView, MainFragment.RecordItemListener recordItemListener) {
                super(itemView);
                mRecordItemListener = recordItemListener;
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int position = getAdapterPosition();
                        Record record = getItem(position);
                        mRecordItemListener.onRecordClick(record);
                    }
                });
            }
        }

        public Record getItem(int position) {
            return mRecordsList.get(position);
        }

        public void pendingRemoval(int position) {
            final Record item = mRecordsList.get(position);
            if (!mPendingRemovalList.contains(item)) {
                mPendingRemovalList.add(item);
                // this will redraw row in "undo" state
                notifyItemChanged(position);
                // let's create, store and post a runnable to remove the item
                Runnable pendingRemovalRunnable = new Runnable() {
                    @Override
                    public void run() {
                        remove(mRecordsList.indexOf(item));
                    }
                };
                handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
                pendingRunnables.put(item, pendingRemovalRunnable);
            }
        }
        public void remove(int position) {
            Record item = mRecordsList.get(position);
            if (mPendingRemovalList.contains(item)) {
                mPendingRemovalList.remove(item);
            }
            if (mRecordsList.contains(item)) {
                Record record = mRecordsList.get(position);
                mRecordItemListener.onRecordSwipe(record);
                notifyItemRemoved(position);
            }
        }
    }

