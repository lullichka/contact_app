package com.example.lullichka.contactapp.recorddetail;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.lullichka.contactapp.R;
import com.example.lullichka.contactapp.addrecord.AddRecordFragment;

public class RecordDetail extends AppCompatActivity {

    private static final String TAG = RecordDetail.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_actionbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle arguments = new Bundle();
        arguments.putParcelable(DetailFragment.REQUESTED_RECORD, getIntent().getParcelableExtra(
                DetailFragment.REQUESTED_RECORD));
        final DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentFrame,detailFragment).commit();
        final FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.fab);
        if (floatingActionButton != null) {
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String name = detailFragment.getTextViewName().getText().toString();
                    String email = detailFragment.getTextViewEmail().getText().toString();
                    String phone = detailFragment.getTextViewPhone().getText().toString();
                    String id = detailFragment.getRecordId();
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.contentFrame, AddRecordFragment.newInstance(id, name, email, phone))
                            .commit();
                    Log.v(TAG, "id is " + String.valueOf(id));
                    floatingActionButton.setImageResource(R.drawable.ic_done_white_24dp);
                }
            });
        }
    }
}
