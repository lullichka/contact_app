package com.example.lullichka.contactapp.recorddetail;

import android.util.Log;

import com.example.lullichka.contactapp.data.RecordRepository;
import com.example.lullichka.contactapp.model.Record;

/**
 * Created by lullichka on 03.10.16.
 */
public class RecordDetailPresenter implements RecordDetailContract.UserActionsListener {

    private static final String LOG_TAG = RecordDetailPresenter.class.getSimpleName();
    private final RecordDetailContract.View mRecordDetailView;
    private final RecordRepository mRecordRepository;

    RecordDetailPresenter(RecordRepository recordRepository,  RecordDetailContract.View recordDetailView) {
        mRecordRepository = recordRepository;
        mRecordDetailView = recordDetailView;
    }
    @Override
    public void openRecord(Record record) {
        if (record == null) {
            Log.v(LOG_TAG, "Record is missing");
            return;
        }
        Log.d(LOG_TAG, "Record is "+record.getId());
        showRecord(record);
    }

    @Override
    public void updateRecord(Record record) {

    }
    private void showRecord(Record record) {
        String name = record.getName();
        String email = record.getEmail();
        String phone = record.getPhone();
        mRecordDetailView.showName(name);
        mRecordDetailView.showEmail(email);
        mRecordDetailView.showPhone(phone);
    }
}
