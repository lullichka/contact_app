package com.example.lullichka.contactapp;

import android.app.Application;
import android.util.Log;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by lullichka on 15.10.16.
 */
public class ContactApp extends Application {
    private static final String TAG = ContactApp.class.getSimpleName();

    @Override
    public void onCreate() {
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        Log.d(TAG, "application created");
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}
