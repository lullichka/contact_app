package com.example.lullichka.contactapp.data;

import com.example.lullichka.contactapp.model.Record;

import java.util.List;

/**
 * Created by lullichka on 14.10.16.
 */
public interface RecordRepoInterface {
    interface LoadRecordsCallback {
        void onRecordsLoaded(List<Record> records);
    }

    void getRecords(LoadRecordsCallback callback);

    void addRecord(Record record);

    void deleteRecord(Record record);

    void updateRecord(Record record);
}
