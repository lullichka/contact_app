package com.example.lullichka.contactapp.recorddetail;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lullichka.contactapp.R;
import com.example.lullichka.contactapp.data.RecordRepository;
import com.example.lullichka.contactapp.model.Record;

/**
 * Created by lullichka on 04.10.16.
 */
public class DetailFragment extends Fragment implements RecordDetailContract.View {
    public static final String REQUESTED_RECORD = "requestedRecord";
    private static final String LOG_TAG = DetailFragment.class.getSimpleName();
    private TextView mTvName;
    private TextView mTvEmail;
    private TextView mTvPhone;
    private RecordDetailPresenter mUserActionsListener;
    private Record mRecord;

    public DetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle arguments = getArguments();
        mRecord = arguments.getParcelable(DetailFragment.REQUESTED_RECORD);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_detail, container, false);
        mTvName = (TextView) rootView.findViewById(R.id.tv_Name);
        mTvEmail = (TextView) rootView.findViewById(R.id.tv_email);
        mTvPhone = (TextView) rootView.findViewById(R.id.tv_phone);
        mUserActionsListener = new RecordDetailPresenter(new RecordRepository(), this);
        mUserActionsListener.openRecord(mRecord);

        return rootView;
    }

    public TextView getTextViewName() {
        return mTvName;
    }

    public TextView getTextViewEmail() {
        return mTvEmail;
    }

    public TextView getTextViewPhone() {
        return mTvPhone;
    }

    public String getRecordId() {
        return mRecord.getId();
    }

    @Override
    public void showName(String name) {
        mTvName.setText(name);
    }

    @Override
    public void showEmail(String email) {
        mTvEmail.setText(email);
    }

    @Override
    public void showPhone(String phone) {
        mTvPhone.setText(phone);
    }
}
