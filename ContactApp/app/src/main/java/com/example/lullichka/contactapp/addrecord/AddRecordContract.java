package com.example.lullichka.contactapp.addrecord;

/**
 * Created by lullichka on 04.10.16.
 */
public interface AddRecordContract {
    interface UserActionsListener {
        void addRecord(String id, String name, String email, String phone);

        void updateRecord(String id, String name, String email, String phone);
    }

    interface View {
        void showRecordsList();

        void showEmptyRecordError();
    }
}
